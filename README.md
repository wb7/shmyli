shmyli
======

SHortMYLInk is a simple URL-Shortener written in PHP and based on mysql.

* Web: http://shmy.li
* Mail: dev@shmy.li (german | english)
* IRC: #shmyli @ freenode.net

Requirements
============

* PHP 5.3 or newer
* Apache webserver (ask your provider, if you don't know what this is)
* mod_rewrite (ask your provider)
* MySQL database

Installation
============

See file INSTALLATION
